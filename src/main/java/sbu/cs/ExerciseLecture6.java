package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {

        long sum = 0L;

        for(int i = 0 ; i<arr.length ; i+=2)
            sum += arr[i];

        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {

        int[] reversed = new int[arr.length] ;
        int reversedArrIndex = arr.length - 1;

        for(int c : arr)
        {
            reversed[reversedArrIndex] = c;
            reversedArrIndex--;
        }

        return reversed;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {

        if(m1[0].length != m2.length)
            throw new RuntimeException() ;

        int row = m1.length;
        int collumn= m2[0].length ;

        double[][] product = new double[row][collumn] ;

        for(int i = 0 ; i<row ; i++)
        {
            for(int j = 0 ; j<collumn ; j++)
            {
                double sum = 0 ;

                for(int p = 0 ; p<m2.length ; p++)
                {
                    sum += m1[i][p] * m2[p][j] ;
                }

                product[i][j] =sum ;
            }
        }


        return product ;

    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {

        List<List<String>> ListofLists= new ArrayList<>();

        for(String[] str : names)
        {
            List<String> list = new ArrayList<>() ;

            for(String s : str)
            {
                list.add(s) ;
            }

            ListofLists.add(list) ;
        }


    return ListofLists ;


    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {

        List<Integer> primes = new ArrayList<Integer>();

        for(int i = 2 ; i<=n ; i++)
            if(n%i==0)
                primes.add(i) ;   // all of the divisible counters will add in this step


        for(int i = 0 ; i<primes.size() ; i++)
        {
            int c = primes.get(i) ;

            for(int j = 2 ; j< c ; j++)
                if(c%j==0)
                {
                    primes.remove(i) ;   // remove the non-prime ones
                    i-=2 ; // the next in list will hava the index with current i value
                    break;
                }
        }


        return primes;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {

        String lowerCases = "abscdefghijklmnopqrstuvwxyz" ;
        String upperCases = lowerCases.toUpperCase();
        String allower = lowerCases + upperCases + " ";


        List<String> words = new ArrayList<String>();

        for (int i = 0; i < line.length(); i++)
            if(!(allower.contains(String.valueOf(line.charAt(i)))))
                line = line.replace( String.valueOf(line.charAt(i)) , "");


        words = Arrays.asList(line.split(" "));

        return words;
    }
}

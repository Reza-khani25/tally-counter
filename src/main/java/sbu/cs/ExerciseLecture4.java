package sbu.cs;

public class ExerciseLecture4 {

    public long factorial(int n) {

        long result = 1L;

        for (long i = 1L ; i <= n ; i++)
                result *= i ;
            
        
        return result;
    }



    public long fibonacci(int n) {

        long[] fib = new long[n];

            fib[0] = 1;

        if(n>1)    // if-Statement fixes the run time Error when n==1 (ArrayIndexOutOfBoundsException)
            fib[1] = 1;

        for (int i = 2; i < n; i++) 
            fib[i] = fib[i-1] +fib[i-2] ;

        return fib[n-1] ;
    }
    
    public String reverse(String word) {

         String result = "" ;

        for (int i = word.length()-1; i >= 0 ; i--) {

            result += String.valueOf(word.charAt(i));

        }
        return result;
    }

    public boolean isPalindrome(String tmp) {

        String line = tmp.replace(" " , "") ;  // remove the whiteSpace's
        line = line.toLowerCase();

        int middleIndex ;

        if(line.length()%2!=0)
            middleIndex = ( line.length() / 2) ;
        else
            middleIndex = ( line.length() / 2) - 1 ;


        for (int i = 0; i <= middleIndex ; i++)
        {
            //charInMirror => initializing the i th character in the condition of reading-from-the-end
            char charInMirror = line.charAt(line.length()-i-1);

            if(line.charAt(i) != charInMirror)
               return false;

        }

        return true;
    }


    public char[][] dotPlot(String str1, String str2) {

        char arr[][] = new char[str1.length()][str2.length()];

        for (int i = 0; i < str1.length(); i++) {

            for (int j = 0; j < str2.length(); j++) {
                if(str1.charAt(i) == str2.charAt(j))
                    arr[i][j] = '*';
                else
                    arr[i][j] = ' ';

            }

        }
        return arr;
   }
}

package sbu.cs;

import java.util.Random;
import java.util.ArrayList;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {

        String lowerLetters = "abcdefghijklmnopqrstuvwxyz" ;

        char[] password = new char[length];
        Random rand = new Random();

        for (int i = 0; i < length; i++)
            password[i] = lowerLetters.charAt(rand.nextInt(25)) ;



        return new String(password);
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {

        if(length<3)
            throw new Exception() ;

        char[] password = new char[length];
        Random rand = new Random();

        String smallLetters = "abcdefghijklmnopqrstuvwxyz" ;   // these Strings can be selected
        String capitalLetters = smallLetters.toUpperCase() ;  //
        String alpha = smallLetters + capitalLetters ;       //
        String specialCharacters = "@#$%^&*-/|}{?." ;       //
        String numbers = "0123456789" ;                    //


        ArrayList <Integer>mandatories = new ArrayList<Integer>();  //for 3 essential choices

        for(int i = 0 ; i<3 ; i++)
        {

            int randomIndex ;
            do
            {
                randomIndex = rand.nextInt(length) ;  // random selection for 3 index'es

            } while((mandatories.contains(randomIndex))); // no duplicate selection

            mandatories.add(randomIndex) ;   // save the random choice  (index)

        }

        // giving value to 3 exceptions
        password[mandatories.get(0)] = alpha.charAt(rand.nextInt(alpha.length()-1)) ;
        password[mandatories.get(1)] = specialCharacters.charAt(rand.nextInt(specialCharacters.length()-1));
        password[mandatories.get(2)] = numbers.charAt(rand.nextInt(9)) ;

        String allAllower = alpha + specialCharacters + numbers ; // the rest can be of any type

        for(int i = 0 ; i<length ; i++)
        {
            if(mandatories.contains(i))   // dont do anything to the exception indexes
                continue ;

            password[i] = allAllower.charAt(rand.nextInt(allAllower.length()));
        }



        return new String(password);

    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin if there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {

        for(int i = 1 ; fib(i)<n ; i++)
        {
            int tmp = fib(i) + Integer.parseInt(Integer.toBinaryString(i)) ;

            if (tmp==n)
            return true  ;
        }

        return false;
    }

    public static int fib(int n){

        int[] fib = new int[n];

        fib[0] = 1;

        if(n>1)    // if-Statement fixes the run time Error when n==1 (ArrayIndexOutOfBoundsException)
            fib[1] = 1;

        for (int i = 2; i < n; i++)
            fib[i] = fib[i-1] +fib[i-2] ;

        return fib[n-1] ;
    }
}
